FROM openjdk:8-jdk

ARG ANDROID_COMPILE_SDK=28
ARG ANDROID_BUILD_TOOLS=28.0.3
ARG ANDROID_SDK_TOOLS=4333796

ENV ANDROID_HOME /android-sdk-linux
ENV PATH $PATH:/opt/gradle/gradle-3.4.1/bin/
ENV PATH $PATH:${ANDROID_HOME}/build-tools/${ANDROID_BUILD_TOOLS}/
ENV PATH $PATH:$(npm bin)


RUN apt-get --quiet update --yes
RUN apt-get --quiet install --yes wget tar unzip lib32stdc++6 lib32z1
RUN wget --quiet --output-document=android-sdk.zip https://dl.google.com/android/repository/sdk-tools-linux-${ANDROID_SDK_TOOLS}.zip
RUN unzip -d android-sdk-linux android-sdk.zip
RUN echo y | android-sdk-linux/tools/bin/sdkmanager "platforms;android-${ANDROID_COMPILE_SDK}" >/dev/null
RUN echo y | android-sdk-linux/tools/bin/sdkmanager "platform-tools" >/dev/null
RUN echo y | android-sdk-linux/tools/bin/sdkmanager "build-tools;${ANDROID_BUILD_TOOLS}" >/dev/null
RUN yes | android-sdk-linux/tools/bin/sdkmanager --licenses

RUN wget https://services.gradle.org/distributions/gradle-3.4.1-bin.zip > /dev/null &&\
    mkdir /opt/gradle &&\
    unzip -d /opt/gradle gradle-3.4.1-bin.zip

RUN curl -sL https://deb.nodesource.com/setup_10.x | bash - &&\
    apt-get install nodejs

RUN npm i -g cordova
RUN npm i -g cordova-res --unsafe-perm=true
